'use strict';

var count = 0; //Изначальное кол-во товаров
var maxCount = 10; //Конечное кол-во товаров
var itemObject ={}; //Обьект товаров

console.log('Введите в консоль createItemObject(),чтобы созать новый товар');
console.log('Введите в консоль searchCategory("Название_категории"),чтобы найти и отсортировать нужный вам обьект');
console.log('Введите в консоль getPrice("название_категории"), чтобы получить цену отсортированных товаров');


function createItemObject() {
    if (count >= maxCount) {
        alert('Больше ' + maxCount + ' нельзя,максимум');
        return;
    }
    var index = prompt('Введите индекс товара, максимум ' + maxCount + ' товаров!', '');
    if (index >= 0 && isNumeric(index)) { // Проверка чиссла на ввод
        if (index in itemObject) { // Если уже существует такой индекс
            alert('Индекс уже занят! Введите функцию заново в консоль!');
            return;
        }
        index = Number(index);
        count++;
    } else {
        alert('Ошибка ввода! Введите функцию заново в консоль!');
        return;
    }
    itemObject[index] = {};
    itemObject[index].name = prompt('Введите название товара. Например: Ручка', ''); // Название товара
    do {
        if (itemObject[index].name !== '' && itemObject[index].name != null) {
            var isErrorInput = false;
        } else {
            isErrorInput = true;
            itemObject[index].name = prompt('Вы не правильно ввели название! Введите название товара повторно. Например: Ручка', '');
        }
    } while(isErrorInput !== false);
    itemObject[index].price = prompt('Введите цену товара. Например: 10', ''); // Цена товара
    do {
        if ( itemObject[index].price > 0 && isNumeric(itemObject[index].price) ) {
            itemObject[index].price = Number(itemObject[index].price);
            isErrorInput = false;
        } else {
            isErrorInput = true;
            itemObject[index].price = prompt('Вы не правильно ввели цену! Введите цену товара повторно. Например: 10', '');
        }
    } while(isErrorInput != false);
    itemObject[index].category = prompt('Введите категорию товара. Например: Канцтовары', ''); // Категория товара
    do {
        if (itemObject[index].category !== '' && itemObject[index].category != null) {
            isErrorInput = false;
        } else {
            isErrorInput = true;
            itemObject[index].category = prompt('Вы не правильно ввели категорию! Введите название категории повторно. Например: Канцтовары', '');
        }
    } while(isErrorInput != false);
    return itemObject;
}

function findCategory(category) { //Функция, которая сортирует по категории старый 'массив' объектов в новый
    var sortItems = {};
    for (var key in itemObject) {
        if (itemObject[key].category === category) {
            sortItems[key] = {};
            sortItems[key].name = itemObject[key].name;
            sortItems[key].price = itemObject[key].price;
            sortItems[key].category = itemObject[key].category;
        }
    }
    if (Object.keys(sortItems).length === 0) {
        alert('Таких товаров нет ');
        return null;
    }
    return sortItems;
}

function getPrice(category) { //Получить общую цену
    var sortItems = findCategory(category);

    if (sortItems === null) {
        return;
    }
    var sum = 0;
    for (var key in sortItems) {
        sum += sortItems[key].price;
    }
    calcDiscount(sum);
}

function calcDiscount(number) { //Высчитать скидку
    if (number > 5) {
        var result = (number - (number / 100 * 12)).toFixed(2);
        console.log('Со скидкой вышло: ' + result);
    } else {
        result = number.toFixed(2);
        console.log('Без скидки вышло: ' + result);
    }
}

function isNumeric(n) { //Проверка на число
    return !isNaN(parseFloat(n)) && isFinite(n);
}
