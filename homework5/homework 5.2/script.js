function f (a = 2, b = 3, c) {
    function sum(a, b) {
        return a + b;
    }
    let s =  sum(a,b);
    return c instanceof Function ? c() : s;
}

console.log(f());
console.log(f(7,8));
console.log(f(9,10, ()=>11));// Для тестирования.