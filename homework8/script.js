'use strict';
console.log("Write in console AddHero() to add new hero");
console.log("Write in console DeleteHero() to delete a hero");
console.log("Write in console InfinityGauntlet() to wipe half of the heroes array");

const heroes = [
    'Captain America',
    'Iron Man',
    'Thor God of Thunder',
    'Scarlet Witch',
    'Black Widow'
];

console.log(heroes);

function AddHero() {
    heroes.push(
        prompt("Write marvel hero name", "Black Panther")
    );
    return heroes;
}

Array.prototype.remove = function(value) {
    let idx = this.indexOf(value);
    if (idx !== -1) {
        // Второй параметр - число элементов, которые необходимо удалить
        return this.splice(idx, 1);
    }
    return false;
};

function DeleteHero() {
    heroes.remove(prompt("Write hero name that you want to delete", "Iron Man"));
    return heroes;
}

function shuffle(heroes) {
    let currentIndex = heroes.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {

        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = heroes[currentIndex];
        heroes[currentIndex] = heroes[randomIndex];
        heroes[randomIndex] = temporaryValue;
    }

}

function InfinityGauntlet() {
    shuffle(heroes);
    let half_length = Math.ceil(heroes.length / 2);
    let leftSide = heroes.splice(0, half_length);
    return heroes
}

heroes.map(hero => hero.length);
