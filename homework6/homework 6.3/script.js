'use strict';

var items = [];
var count = 0;

console.log('Введите в консоль: createItem("Название_товара", "Цену", "Категорию_товара"), чтобы создать товар.');
console.log('Введите в консоль: showItems(), чтобы посмотреть исходный массив товаров');
console.log('Введите в консоль: filterPrice("От", "До"), чтобы отфильтровать цену товара.');
console.log('Введите в консоль: filterCategory("Категория"), чтобы отфильтровать товары по категории.');
console.log('Введите в консоль: findLengthCategory("Категория"), чтобы посчитать ков-во товаров в категории.');
console.log('Введите в консоль: deleteItem("Название_товара"), чтобы удалить товар.');
console.log('Введите в консоль: sortByPrice("+"), чтобы отсортировать товар по возрастанию или sortByPrice("-") по убыванию');
console.log('Введите в консоль: sortAndFilter("+", "Категория"), чтобы отсортировать и отфильтровать товар по возрастанию и категории или sortAndFilter("-", "От", "До"), чтобы отсортировать и отфильтровать товар по убыванию и цене');
console.log('Введите в консоль: filterAndSum("Категория"), чтобы отфильтровать по категории и получить сумму цен товаров. Либо filterAndSum("От", "До") чтобы отфильтровать по цене и получить сумму цен товаров');
console.log('----------------------Аргументы вводите всегда в кавичках ""---------------------------------------------');

createItem("Силовой кабель", "188", "Техника");
createItem("Кресло", "2700", "Мебель");
createItem("Компьютерная мышь", "150", "Техника");
createItem("Монитор", "2300", "Мебель");
createItem("Клавиатура", "350", "Техника");
createItem("Яблоко", "10", "Продукты");
createItem("Диван", "3600", "Мебель");

function createItem(name, price, category) { //1
    if (!name || !isNumeric(price) || price <= 0 || !category ) {
        console.log('Не правильно введены аргументы. Try again');
        return;
    }
    count++;
    var currentIndex = count - 1;
    items[currentIndex] = {};
    items[currentIndex].name = name;
    items[currentIndex].price = +price;
    items[currentIndex].category = category;
    return items;
}

function showItems() { //Вывод информации
    console.log(items);
}

function filterPrice(min, max, arr) { // 2
    if (!isNumeric(min) || !isNumeric(max) || min < 0 || +max < +min) { // Проверка на ввод
        console.log('Не правильно введены аргументы. Try again');
        return;
    }
    min = +min;
    max = +max;
    var newItems = (arr === undefined) ? items.filter(comparePrice) : arr.filter(comparePrice); // Если не передали массив аргументом, то фильтруем исходный
    return newItems;
    function comparePrice(value) {
        if (value.price >= min && value.price <= max) {
            return true;
        }
    }
}

function filterCategory(category, arr) { //3
    if(!category) {
        console.log('Не правильно введены аргументы. Try again');
        return;
    }

    var newItems = (arr === undefined) ? items.filter(compareCategory) : arr.filter(compareCategory); // Если не передали аргументом массив, то фильтруем исходный
    return newItems;


    function compareCategory(value) {
        if (value.category === category) {
            return true;
        }
    }
}

function findLengthCategory(category) { //4
    var newItems = filterCategory(category);
    return (newItems !== undefined) ? newItems.length : undefined;
}

function deleteItem(name) { //5
    if (!name) {
        console.log('Не правильно введены аргументы. Try again');
        return;
    }

    var isDeleted = false;

    items = items.filter(delItem);
    (isDeleted) ? console.log('Товар удален') : console.log('Товар не найден!');

    function delItem(value) {
        if (value.name === name) {
            isDeleted = true;
            return false;
        }

        return true;
    }
}

function sortByPrice(direction) { //6
    var newItems = [];


    switch (direction) {
        case '+':
            return newItems = items.sort(sortAscending);
        case '-':
            return newItems = items.sort(sortDescending);
        default : return console.log('Не правильно введены аргументы. Try again');
    }


    function sortAscending(a, b) {
        return a.price - b.price;
    }

    function sortDescending(a, b) {
        return b.price - a.price;
    }
}

function sortAndFilter(direction, value, secondValue) {// 7
    if ((direction !== '+' && direction !== '-') || !value) {
        console.log('Не правильно введены аргументы. Try again');
        return;
    }

    var newItems = [];
    newItems = sortByPrice(direction);

    if ( isNumeric(value) && isNumeric(secondValue) ) {
        return newItems = filterPrice(value, secondValue, newItems);
    } else if (!isNumeric(value) && secondValue === undefined) {
        return newItems = filterCategory(value, newItems);
    } else {
        console.log('Не правильно ввели аргументы функции! Попробуйте еще раз.');
        return;
    }
}

function filterAndSum(value, secondValue) { // 8

    var newItems = [];

    if ( isNumeric(value) && isNumeric(secondValue) ) {
        newItems = filterPrice(value, secondValue);
    } else if (!isNumeric(value) && secondValue === undefined) {
        newItems = filterCategory(value);
    } else {
        console.log('Не правильно введены аргументы. Try again');
        return;
    }

    if (newItems === undefined) {
        return;
    }

    var result = newItems.reduce(SumArr, 0);
    return result;


    function SumArr(sum, current) {
        return sum + current.price;
    }
}

function isNumeric(number) {
    return !isNaN(parseFloat(number)) && isFinite(number);
}