'use strict';

var minCount = 3; //Минимальное число для ввода
var maxCount = 10;//Максимальное число для ввода
var maxNumber = 100;//Максимальное суммарное число ввода
var str = prompt ('Введите числа через запятую, минимум ' + minCount + ' максимум ' + maxCount + ' но не более ' + maxNumber);

var arr = (str===null) ? [] : str.split(', ');//Преобразование строки в массив
arr = arr.map (strToNumberArr); //Преобразование его в числовой тип

if (  isValidArr(arr, minCount, maxCount, maxNumber) ) {
    arr.sort(sortNumbers);//Сортировка по возрастанию чисел
    var result = arr.join(', ');//Преобразование массива в строку
    console.log('Результат сортировки чисел по возврастанию: ' + result);

    arr.sort(sortByEven);//Сортировка по четным числам
    var secondResult = arr.join(', ');//Преобразование массива в строку
    console.log('Результат сортировки четных чисел: ' + secondResult);
} else {
    alert('Вы не правильно ввели данные. Перезагрузите страницу');
}

function strToNumberArr (value) {
    if (value === '') {
        return NaN;
    }
    return +value;
}

function isValidArr(arr, minCount, maxCount) { //Проверка чисел в массиве и максимального суммарного числа.
    if (arr.length < minCount || arr.length > maxCount) { //Проверка кол-ва элементов
        return false;
    }
    return arr.every(isValidNum);//Проверка на наличие чисел и максимального значения

}

function isValidNum(number) {
    return ( (!isNaN(parseFloat(number)) && isFinite(number))
        && number <= maxNumber );
}

function sortNumbers(a, b) {
    return a - b;
}

function sortByEven(a, b) {
    if (a % 2 === 0 && b % 2 === 0) {
        return a - b;
    }
    if (a % 2 === 0 && b % 2 !== 0) {
        return -1;
    }
    if (a % 2 !== 0 && b % 2 === 0) {
        return 1;
    }
    if (a % 2 !== 0 && b % 2 !== 0) {
        return a - b;
    }
}
